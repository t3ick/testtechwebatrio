<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
//use Doctrine\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Personne;


class PersonneController extends AbstractController
{
    /**
     * @Route("/personne", name="personne")
     */
    public function index(EntityManagerInterface $manager, Request $request): Response
    {

        $personne = new Personne();
        $form = $this->createFormBuilder($personne)
            ->add('nom')
            ->add('prenom')
            ->add('valider',SubmitType::class, [
                'label' => 'Enregistrer'
            ])
            ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($personne);
            $manager->flush();
        }

        $repo = $this->getDoctrine()->getRepository(Personne::class);
        $aPersonnes = $repo
            ->findBy([], ['nom' => 'ASC']);

        ;

        return $this->render('personne/formPersonne.html.twig', [
            'formInscription'   => $form->createView(),
            'aPersonnes'        => $aPersonnes
        ]);
    }
}
