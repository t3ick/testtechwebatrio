// Init project
# composer create-project symfony/website-skeleton AtrioTestTec

// depui le dossier racine du projet symfony : AtrioTestTec

// génération du controller personne
# php bin/console make:controller PersonneController


// add .htacces pour rediriger les route sur le public/index.php
# pcomposer require symfony/apache-pack

// génère l'entity personne
# php bin/console make:entity